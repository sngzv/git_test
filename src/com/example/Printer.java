package com.example;

public class Printer {

    private String message;

    public Printer(String message) {
        this.message = message;
    }

    public void print() {
        System.out.println("Your message: " + message);
    }

    public void print(String msg) {
        System.out.println(msg);
    }
}
